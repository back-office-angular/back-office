import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { TableEmployeeComponent } from './table-employee/table-employee.component';
import { PaginationComponent } from './pagination/pagination.component';
import { PageEmployeeComponent } from './page-employee/page-employee.component';
import { PageLoginComponent } from './page-login/page-login.component';
import { PageEmployeeAddComponent } from './page-employee-add/page-employee-add.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PageEmployeeDetailComponent } from './page-employee-detail/page-employee-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    TableEmployeeComponent,
    PaginationComponent,
    PageEmployeeComponent,
    PageLoginComponent,
    PageEmployeeAddComponent,
    PageEmployeeDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatAutocompleteModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
