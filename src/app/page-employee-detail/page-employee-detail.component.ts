import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { Employee } from '../employee';

@Component({
  selector: 'app-page-employee-detail',
  templateUrl: './page-employee-detail.component.html',
  styleUrls: ['./page-employee-detail.component.css']
})
export class PageEmployeeDetailComponent implements OnInit {
  loginFlag: boolean;
  username;
  employees;
  employee;

  constructor(private route: ActivatedRoute, private empService: EmployeeService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.loginFlag = (params.get("loginFlag")=="true");
      this.username = params.get("employeeId");
    });

    this.empService.getEmployees().subscribe(data=>{
      this.employees = data;
      for (var i = 0; i < this.employees.length; i++){
        if (this.employees[i].username == this.username){
          this.employee = this.employees[i];
        }
      }
    });
  }

}
