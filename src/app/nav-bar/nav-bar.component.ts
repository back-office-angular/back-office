import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Input()
  loginFlag: boolean;
  
  constructor(private router: Router) { 
  }

  ngOnInit(): void {
  }

  backToLogin(){
    this.router.navigate(['']);
  }
}
