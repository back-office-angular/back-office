import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-page-employee-add',
  templateUrl: './page-employee-add.component.html',
  styleUrls: ['./page-employee-add.component.css']
})
export class PageEmployeeAddComponent implements OnInit {
  loginFlag: boolean;
  maxDate: Date;
  formdata;
  //attribute for dropdown
  group = new FormControl();
  options: string[] = ['IT Developer', 'IT Network', 'IT Operation', 'HR', 'Legal', 'Operation', 'Finance', 'Audit', 'Security', 'Business'];
  filteredOptions: Observable<string[]>;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.loginFlag = (params.get("loginFlag")=="true");
    });

    //validate form and intialitation formData
    this.formdata = new FormGroup({
      username: new FormControl("username",[
        Validators.required
      ]),
      firstname: new FormControl("firstname",[
        Validators.required
      ]),
      lastname: new FormControl("lastname",[
        Validators.required
      ]),
      email: new FormControl("email@go.com",[
        Validators.required,
        Validators.pattern("[^ @]*@[^ @]*")
      ]),
      birthdate: new FormControl("",[
        Validators.required
      ]),
      basicsalary: new FormControl("1000000",[
        Validators.required,
        Validators.pattern("^[1-9][0-9]*$")
      ]),
      status: new FormControl("status",[
        Validators.required
      ]),
      description: new FormControl("description",[
        Validators.required
      ])
    });

    //validate birth lower than current date
    const currentDate = new Date();
    this.maxDate = new Date(currentDate);

    //filter options
    this.filteredOptions = this.group.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value.toString()))
      );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter(option => option.toLowerCase().includes(filterValue));
  }

  get username() { return this.formdata.get('username'); }

  get firstname() { return this.formdata.get('firstname'); }

  get lastname() { return this.formdata.get('lastname'); }

  get email() { return this.formdata.get('email'); }

  get birthdate() { return this.formdata.get('birthdate'); }

  get basicsalary() { return this.formdata.get('basicsalary'); }

  get status() { return this.formdata.get('status'); }

  get description() { return this.formdata.get('description'); }

  onClickSubmit(data) {

  }
}
