import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEmployeeAddComponent } from './page-employee-add.component';

describe('PageEmployeeAddComponent', () => {
  let component: PageEmployeeAddComponent;
  let fixture: ComponentFixture<PageEmployeeAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEmployeeAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEmployeeAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
