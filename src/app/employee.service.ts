import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Employee } from '../app/employee';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  jsonPath: string;

  constructor(
    private http: HttpClient
  ) 
  { this.jsonPath='/assets/employee.json' }

  getEmployees() {
    return this.http.get(this.jsonPath);  
  }
}
