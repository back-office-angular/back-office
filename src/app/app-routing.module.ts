import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageLoginComponent } from './page-login/page-login.component';
import { PageEmployeeComponent } from './page-employee/page-employee.component';
import { PageEmployeeAddComponent } from './page-employee-add/page-employee-add.component';
import { PageEmployeeDetailComponent } from './page-employee-detail/page-employee-detail.component';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: '', component: PageLoginComponent },
    { path: 'employees/:loginFlag', component: PageEmployeeComponent },
    { path: 'employeeAdd/:loginFlag', component: PageEmployeeAddComponent },
    { path: 'employeeDetail/:loginFlag/:employeeId', component: PageEmployeeDetailComponent }
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
