import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-page-employee',
  templateUrl: './page-employee.component.html',
  styleUrls: ['./page-employee.component.css']
})
export class PageEmployeeComponent implements OnInit {
  loginFlag: boolean;
  public flagEdit: boolean = false;
  public flagDelete: boolean = false;
  
  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.loginFlag = (params.get("loginFlag")=="true");
    });
  }

  //allert handle
  declareAlertFlag(flag: string){
    if(flag=="flagEdit"){
      setTimeout(() => {
        this.flagEdit=true;
        this.flagDelete=false;
      }, 1000)
    }
    if(flag=="flagDelete"){
      setTimeout(() => {
        this.flagDelete=true;
        this.flagEdit=false;
      }, 1000)
    }
  }
}
