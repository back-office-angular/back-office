import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageEmployeeComponent } from './page-employee.component';

describe('PageEmployeeComponent', () => {
  let component: PageEmployeeComponent;
  let fixture: ComponentFixture<PageEmployeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageEmployeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageEmployeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
