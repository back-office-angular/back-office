import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page-login',
  templateUrl: './page-login.component.html',
  styleUrls: ['./page-login.component.css']
})
export class PageLoginComponent implements OnInit {
  loginFlag: boolean;
  invalidFlag: boolean;
  formdata;

  constructor(private router: Router) {
    this.loginFlag=false;
  }

  ngOnInit(): void {
    this.formdata = new FormGroup({
      username: new FormControl("username@gmail.com",[
        Validators.required,
        Validators.pattern("[^ @]*@[^ @]*")
      ]),
      password: new FormControl("password",[
        Validators.required
      ])
   });
  }

  get username() { return this.formdata.get('username'); }

  get password() { return this.formdata.get('password'); }

  onClickSubmit(data) {
    if(data.username=="abcd@gmail.com" && data.password=="abcd"){
      this.loginFlag= true;
      this.router.navigate(['/employees', this.loginFlag]);
    }
    this.invalidFlag=true;
  }
}
