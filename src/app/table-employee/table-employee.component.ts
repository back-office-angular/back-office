import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-table-employee',
  templateUrl: './table-employee.component.html',
  styleUrls: ['./table-employee.component.css']
})
export class TableEmployeeComponent implements OnInit {
  @Input()
  loginFlag: boolean;
  employees: any = [];
  @Output() onClickedAction = new EventEmitter <string>();

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.getEmployees().subscribe(data=>{
      this.employees=data;
    });
  }

  showEditNotif(){
    this.showNotif("flagEdit");
  }

  showDeleteNotif(){
    this.showNotif("flagDelete");
  }

  //Declare emit for output to app-page-employee
  showNotif(flag: string){
    this.onClickedAction.emit(flag);
  }

}
